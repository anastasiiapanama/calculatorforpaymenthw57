import React, {useState} from 'react';
import './App.css';


const App = () => {
    const [node, setNode] = useState('first');
    const [orderItems, setOrderItems] = useState([{orderPerson: '', orderPrice: '', orderTip: '', orderDelivery: ''}]);
    const [items, setItems] = useState([{name: '', price: '', tip: '', delivery: ''}]);

    const onRadioChange = (e) => {
        setNode(e.target.value);
    };

    const onChangeOrderPerson = (e, index) => {
        const orderItemsCopy = [...orderItems];
        const orderItemCopy = {...orderItemsCopy[index]};
        orderItemCopy.orderPerson = e.target.value;
        orderItemsCopy[index] = orderItemCopy;

        setOrderItems(orderItemsCopy);
    };

    const onChangeOrderPrice = (e, index) => {
        const orderItemsCopy = [...orderItems];
        const orderItemCopy = {...orderItemsCopy[index]};
        orderItemCopy.orderPrice = e.target.value;
        orderItemsCopy[index] = orderItemCopy;

        setOrderItems(orderItemsCopy);
    };

    const onChangeOrderTip = (e, index) => {
        const orderItemsCopy = [...orderItems];
        const orderItemCopy = {...orderItemsCopy[index]};
        orderItemCopy.orderTip = e.target.value;
        orderItemsCopy[index] = orderItemCopy;

        setOrderItems(orderItemsCopy);
    };

    const onChangeOrderDelivery = (e, index) => {
        const orderItemsCopy = [...orderItems];
        const orderItemCopy = {...orderItemsCopy[index]};
        orderItemCopy.orderDelivery = e.target.value;
        orderItemsCopy[index] = orderItemCopy;

        setOrderItems(orderItemsCopy);
    };

    const orderSummary = ({orderItems}) => {
        const orderItem = [];
        let totalSum = 0;

        Object.keys(orderItems).forEach(itemKey => {
            const itemPrice = (orderItems[itemKey].orderTip * orderItems[itemKey].orderPrice) / 100 + orderItems[itemKey].orderPrice;
            const onePays = Math.ceil(totalSum / orderItems[itemKey].orderPerson);

            totalSum += itemPrice;

            orderItem.push((
                <div key={itemKey}>
                    <p>Общая сумма: <strong>{totalSum}</strong></p>
                    <p>Количество человек: <strong>{orderItems[itemKey].orderPerson}</strong></p>
                    <p>Каждый платит по: <strong>{onePays}</strong> </p>
                </div>
            ));
        });

        return (
            <div>
                {orderItem}
            </div>
        )
    };

    const addItem = () => {
        setItems([...items, {name: '', price: ''}]);
    };

    const onChangeName = (e, index) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[index]};
        itemCopy.name = e.target.value;
        itemsCopy[index] = itemCopy;

        setItems(itemsCopy);
    };

    const onChangePrice = (e, index) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[index]};
        itemCopy.price = e.target.value;
        itemsCopy[index] = itemCopy;

        setItems(itemsCopy);
    };

    const onChangeTip = (e, index) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[index]};
        itemCopy.tip = e.target.value;
        itemsCopy[index] = itemCopy;

        setItems(itemsCopy);
    };

    const onChangeDelivery = (e, index) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[index]};
        itemCopy.delivery = e.target.value;
        itemsCopy[index] = itemCopy;

        setItems(itemsCopy);
    };

    return (
        <div className="container">
            <p><strong>Сумма заказа считается:</strong></p>
            <p>
                <input
                    type="radio"
                    name="options"
                    value="first"
                    onChange={onRadioChange}
                    checked={node === 'first'}
                /> Поровну между всеми участниками
            </p>

            <p>
                <input
                    type="radio"
                    name="options"
                    value="second"
                    onChange={onRadioChange}
                    checked={node === 'second'}
                /> Каждому индивидуально
            </p>

            <p>
                {node === 'first' && (
                    <div>
                        {orderItems.map((orderItem, index) => (
                            <div key={index}>
                                <p>Человек: <input type="text" value={orderItem.orderPerson} onChange={e => onChangeOrderPerson(e, index)}/> чел.</p>
                                <p>Сумма заказа: <input type="text" value={orderItem.orderPrice} onChange={e => onChangeOrderPrice(e, index)}/> сом</p>
                                <p>Процент чаевых: <input type="text" value={orderItem.orderTip} onChange={e => onChangeOrderTip(e, index)}/> %</p>
                                <p>Доставка: <input type="text" value={orderItem.orderDelivery} onChange={e => onChangeOrderDelivery(e, index)}/> сом</p>
                            </div>
                        ))}

                        <button onClick={orderSummary}> Рассчитать</button>
                    </div>
                )}

                {node === 'second' && (
                    <div>
                        {items.map((item, index) => (
                            <div key={index}>
                                <input type="text" value={item.name} placeholder="Name" onChange={e => onChangeName(e, index)}/>&nbsp;
                                <input type="text" value={item.price} placeholder="Price" onChange={e => onChangePrice(e, index)}/>&nbsp;
                                <button>-</button>
                            </div>
                        ))}

                        <button onClick={addItem}>+</button>

                        <div>
                            {items.map((item, index) => (
                                <div key={index}>
                                    <p>Процент чаевых: <input type="text" value={item.tip} onChange={e => onChangeTip(e, index)}/> %</p>
                                    <p>Доставка: <input type="text" value={item.delivery} onChange={e => onChangeDelivery(e, index)}/> сом</p>
                                </div>
                            ))}
                        </div>
                        <button>Рассчитать</button>
                    </div>
                )}
            </p>
        </div>
    )

}

export default App;
